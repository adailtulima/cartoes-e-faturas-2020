import { TestBed, getTestBed } from '@angular/core/testing';
import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';
import { CartaoService } from 'app/entities/cartao/cartao.service';
import { ICartao, Cartao } from 'app/shared/model/cartao.model';
import { Status } from 'app/shared/model/enumerations/status.model';

describe('Service Tests', () => {
  describe('Cartao Service', () => {
    let injector: TestBed;
    let service: CartaoService;
    let httpMock: HttpTestingController;
    let elemDefault: ICartao;
    let expectedResult: ICartao | ICartao[] | boolean | null;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [HttpClientTestingModule]
      });
      expectedResult = null;
      injector = getTestBed();
      service = injector.get(CartaoService);
      httpMock = injector.get(HttpTestingController);

      elemDefault = new Cartao(0, 'AAAAAAA', 0, 0, 'AAAAAAA', 0, Status.BLOQUEADO, 0);
    });

    describe('Service methods', () => {
      it('should find an element', () => {
        const returnedFromService = Object.assign({}, elemDefault);

        service.find(123).subscribe(resp => (expectedResult = resp.body));

        const req = httpMock.expectOne({ method: 'GET' });
        req.flush(returnedFromService);
        expect(expectedResult).toMatchObject(elemDefault);
      });

      it('should create a Cartao', () => {
        const returnedFromService = Object.assign(
          {
            id: 0
          },
          elemDefault
        );

        const expected = Object.assign({}, returnedFromService);

        service.create(new Cartao()).subscribe(resp => (expectedResult = resp.body));

        const req = httpMock.expectOne({ method: 'POST' });
        req.flush(returnedFromService);
        expect(expectedResult).toMatchObject(expected);
      });

      it('should update a Cartao', () => {
        const returnedFromService = Object.assign(
          {
            numero: 'BBBBBB',
            mesValidade: 1,
            anoValidade: 1,
            nome: 'BBBBBB',
            codigoSeguranca: 1,
            status: 'BBBBBB',
            limiteCreditos: 1
          },
          elemDefault
        );

        const expected = Object.assign({}, returnedFromService);

        service.update(expected).subscribe(resp => (expectedResult = resp.body));

        const req = httpMock.expectOne({ method: 'PUT' });
        req.flush(returnedFromService);
        expect(expectedResult).toMatchObject(expected);
      });

      it('should return a list of Cartao', () => {
        const returnedFromService = Object.assign(
          {
            numero: 'BBBBBB',
            mesValidade: 1,
            anoValidade: 1,
            nome: 'BBBBBB',
            codigoSeguranca: 1,
            status: 'BBBBBB',
            limiteCreditos: 1
          },
          elemDefault
        );

        const expected = Object.assign({}, returnedFromService);

        service.query().subscribe(resp => (expectedResult = resp.body));

        const req = httpMock.expectOne({ method: 'GET' });
        req.flush([returnedFromService]);
        httpMock.verify();
        expect(expectedResult).toContainEqual(expected);
      });

      it('should delete a Cartao', () => {
        service.delete(123).subscribe(resp => (expectedResult = resp.ok));

        const req = httpMock.expectOne({ method: 'DELETE' });
        req.flush({ status: 200 });
        expect(expectedResult);
      });
    });

    afterEach(() => {
      httpMock.verify();
    });
  });
});
