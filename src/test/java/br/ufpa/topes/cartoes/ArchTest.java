package br.ufpa.topes.cartoes;

import static com.tngtech.archunit.lang.syntax.ArchRuleDefinition.noClasses;

import com.tngtech.archunit.core.domain.JavaClasses;
import com.tngtech.archunit.core.importer.ClassFileImporter;
import com.tngtech.archunit.core.importer.ImportOption;
import org.junit.jupiter.api.Test;

class ArchTest {

    @Test
    void servicesAndRepositoriesShouldNotDependOnWebLayer() {
        JavaClasses importedClasses = new ClassFileImporter()
            .withImportOption(ImportOption.Predefined.DO_NOT_INCLUDE_TESTS)
            .importPackages("br.ufpa.topes.cartoes");

        noClasses()
            .that()
            .resideInAnyPackage("br.ufpa.topes.cartoes.service..")
            .or()
            .resideInAnyPackage("br.ufpa.topes.cartoes.repository..")
            .should()
            .dependOnClassesThat()
            .resideInAnyPackage("..br.ufpa.topes.cartoes.web..")
            .because("Services and repositories should not depend on web layer")
            .check(importedClasses);
    }
}
