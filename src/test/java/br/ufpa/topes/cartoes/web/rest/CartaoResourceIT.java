package br.ufpa.topes.cartoes.web.rest;

import br.ufpa.topes.cartoes.ProjetoCartoesEFaturasApp;
import br.ufpa.topes.cartoes.domain.Cartao;
import br.ufpa.topes.cartoes.repository.CartaoRepository;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.transaction.annotation.Transactional;
import javax.persistence.EntityManager;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

import br.ufpa.topes.cartoes.domain.enumeration.Status;
/**
 * Integration tests for the {@link CartaoResource} REST controller.
 */
@SpringBootTest(classes = ProjetoCartoesEFaturasApp.class)

@AutoConfigureMockMvc
@WithMockUser
public class CartaoResourceIT {

    private static final String DEFAULT_NUMERO = "AAAAAAAAAA";
    private static final String UPDATED_NUMERO = "BBBBBBBBBB";

    private static final Integer DEFAULT_MES_VALIDADE = 1;
    private static final Integer UPDATED_MES_VALIDADE = 2;

    private static final Integer DEFAULT_ANO_VALIDADE = 1;
    private static final Integer UPDATED_ANO_VALIDADE = 2;

    private static final String DEFAULT_NOME = "AAAAAAAAAA";
    private static final String UPDATED_NOME = "BBBBBBBBBB";

    private static final Integer DEFAULT_CODIGO_SEGURANCA = 1;
    private static final Integer UPDATED_CODIGO_SEGURANCA = 2;

    private static final Status DEFAULT_STATUS = Status.BLOQUEADO;
    private static final Status UPDATED_STATUS = Status.LIBERADO;

    private static final Double DEFAULT_LIMITE_CREDITOS = 1D;
    private static final Double UPDATED_LIMITE_CREDITOS = 2D;

    @Autowired
    private CartaoRepository cartaoRepository;

    @Autowired
    private EntityManager em;

    @Autowired
    private MockMvc restCartaoMockMvc;

    private Cartao cartao;

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static Cartao createEntity(EntityManager em) {
        Cartao cartao = new Cartao()
            .numero(DEFAULT_NUMERO)
            .mesValidade(DEFAULT_MES_VALIDADE)
            .anoValidade(DEFAULT_ANO_VALIDADE)
            .nome(DEFAULT_NOME)
            .codigoSeguranca(DEFAULT_CODIGO_SEGURANCA)
            .status(DEFAULT_STATUS)
            .limiteCreditos(DEFAULT_LIMITE_CREDITOS);
        return cartao;
    }
    /**
     * Create an updated entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static Cartao createUpdatedEntity(EntityManager em) {
        Cartao cartao = new Cartao()
            .numero(UPDATED_NUMERO)
            .mesValidade(UPDATED_MES_VALIDADE)
            .anoValidade(UPDATED_ANO_VALIDADE)
            .nome(UPDATED_NOME)
            .codigoSeguranca(UPDATED_CODIGO_SEGURANCA)
            .status(UPDATED_STATUS)
            .limiteCreditos(UPDATED_LIMITE_CREDITOS);
        return cartao;
    }

    @BeforeEach
    public void initTest() {
        cartao = createEntity(em);
    }

    @Test
    @Transactional
    public void createCartao() throws Exception {
        int databaseSizeBeforeCreate = cartaoRepository.findAll().size();

        // Create the Cartao
        restCartaoMockMvc.perform(post("/api/cartaos")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(cartao)))
            .andExpect(status().isCreated());

        // Validate the Cartao in the database
        List<Cartao> cartaoList = cartaoRepository.findAll();
        assertThat(cartaoList).hasSize(databaseSizeBeforeCreate + 1);
        Cartao testCartao = cartaoList.get(cartaoList.size() - 1);
        assertThat(testCartao.getNumero()).isEqualTo(DEFAULT_NUMERO);
        assertThat(testCartao.getMesValidade()).isEqualTo(DEFAULT_MES_VALIDADE);
        assertThat(testCartao.getAnoValidade()).isEqualTo(DEFAULT_ANO_VALIDADE);
        assertThat(testCartao.getNome()).isEqualTo(DEFAULT_NOME);
        assertThat(testCartao.getCodigoSeguranca()).isEqualTo(DEFAULT_CODIGO_SEGURANCA);
        assertThat(testCartao.getStatus()).isEqualTo(DEFAULT_STATUS);
        assertThat(testCartao.getLimiteCreditos()).isEqualTo(DEFAULT_LIMITE_CREDITOS);
    }

    @Test
    @Transactional
    public void createCartaoWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = cartaoRepository.findAll().size();

        // Create the Cartao with an existing ID
        cartao.setId(1L);

        // An entity with an existing ID cannot be created, so this API call must fail
        restCartaoMockMvc.perform(post("/api/cartaos")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(cartao)))
            .andExpect(status().isBadRequest());

        // Validate the Cartao in the database
        List<Cartao> cartaoList = cartaoRepository.findAll();
        assertThat(cartaoList).hasSize(databaseSizeBeforeCreate);
    }


    @Test
    @Transactional
    public void getAllCartaos() throws Exception {
        // Initialize the database
        cartaoRepository.saveAndFlush(cartao);

        // Get all the cartaoList
        restCartaoMockMvc.perform(get("/api/cartaos?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(cartao.getId().intValue())))
            .andExpect(jsonPath("$.[*].numero").value(hasItem(DEFAULT_NUMERO)))
            .andExpect(jsonPath("$.[*].mesValidade").value(hasItem(DEFAULT_MES_VALIDADE)))
            .andExpect(jsonPath("$.[*].anoValidade").value(hasItem(DEFAULT_ANO_VALIDADE)))
            .andExpect(jsonPath("$.[*].nome").value(hasItem(DEFAULT_NOME)))
            .andExpect(jsonPath("$.[*].codigoSeguranca").value(hasItem(DEFAULT_CODIGO_SEGURANCA)))
            .andExpect(jsonPath("$.[*].status").value(hasItem(DEFAULT_STATUS.toString())))
            .andExpect(jsonPath("$.[*].limiteCreditos").value(hasItem(DEFAULT_LIMITE_CREDITOS.doubleValue())));
    }
    
    @Test
    @Transactional
    public void getCartao() throws Exception {
        // Initialize the database
        cartaoRepository.saveAndFlush(cartao);

        // Get the cartao
        restCartaoMockMvc.perform(get("/api/cartaos/{id}", cartao.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.id").value(cartao.getId().intValue()))
            .andExpect(jsonPath("$.numero").value(DEFAULT_NUMERO))
            .andExpect(jsonPath("$.mesValidade").value(DEFAULT_MES_VALIDADE))
            .andExpect(jsonPath("$.anoValidade").value(DEFAULT_ANO_VALIDADE))
            .andExpect(jsonPath("$.nome").value(DEFAULT_NOME))
            .andExpect(jsonPath("$.codigoSeguranca").value(DEFAULT_CODIGO_SEGURANCA))
            .andExpect(jsonPath("$.status").value(DEFAULT_STATUS.toString()))
            .andExpect(jsonPath("$.limiteCreditos").value(DEFAULT_LIMITE_CREDITOS.doubleValue()));
    }

    @Test
    @Transactional
    public void getNonExistingCartao() throws Exception {
        // Get the cartao
        restCartaoMockMvc.perform(get("/api/cartaos/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateCartao() throws Exception {
        // Initialize the database
        cartaoRepository.saveAndFlush(cartao);

        int databaseSizeBeforeUpdate = cartaoRepository.findAll().size();

        // Update the cartao
        Cartao updatedCartao = cartaoRepository.findById(cartao.getId()).get();
        // Disconnect from session so that the updates on updatedCartao are not directly saved in db
        em.detach(updatedCartao);
        updatedCartao
            .numero(UPDATED_NUMERO)
            .mesValidade(UPDATED_MES_VALIDADE)
            .anoValidade(UPDATED_ANO_VALIDADE)
            .nome(UPDATED_NOME)
            .codigoSeguranca(UPDATED_CODIGO_SEGURANCA)
            .status(UPDATED_STATUS)
            .limiteCreditos(UPDATED_LIMITE_CREDITOS);

        restCartaoMockMvc.perform(put("/api/cartaos")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(updatedCartao)))
            .andExpect(status().isOk());

        // Validate the Cartao in the database
        List<Cartao> cartaoList = cartaoRepository.findAll();
        assertThat(cartaoList).hasSize(databaseSizeBeforeUpdate);
        Cartao testCartao = cartaoList.get(cartaoList.size() - 1);
        assertThat(testCartao.getNumero()).isEqualTo(UPDATED_NUMERO);
        assertThat(testCartao.getMesValidade()).isEqualTo(UPDATED_MES_VALIDADE);
        assertThat(testCartao.getAnoValidade()).isEqualTo(UPDATED_ANO_VALIDADE);
        assertThat(testCartao.getNome()).isEqualTo(UPDATED_NOME);
        assertThat(testCartao.getCodigoSeguranca()).isEqualTo(UPDATED_CODIGO_SEGURANCA);
        assertThat(testCartao.getStatus()).isEqualTo(UPDATED_STATUS);
        assertThat(testCartao.getLimiteCreditos()).isEqualTo(UPDATED_LIMITE_CREDITOS);
    }

    @Test
    @Transactional
    public void updateNonExistingCartao() throws Exception {
        int databaseSizeBeforeUpdate = cartaoRepository.findAll().size();

        // Create the Cartao

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restCartaoMockMvc.perform(put("/api/cartaos")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(cartao)))
            .andExpect(status().isBadRequest());

        // Validate the Cartao in the database
        List<Cartao> cartaoList = cartaoRepository.findAll();
        assertThat(cartaoList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    public void deleteCartao() throws Exception {
        // Initialize the database
        cartaoRepository.saveAndFlush(cartao);

        int databaseSizeBeforeDelete = cartaoRepository.findAll().size();

        // Delete the cartao
        restCartaoMockMvc.perform(delete("/api/cartaos/{id}", cartao.getId())
            .accept(MediaType.APPLICATION_JSON))
            .andExpect(status().isNoContent());

        // Validate the database contains one less item
        List<Cartao> cartaoList = cartaoRepository.findAll();
        assertThat(cartaoList).hasSize(databaseSizeBeforeDelete - 1);
    }
}
