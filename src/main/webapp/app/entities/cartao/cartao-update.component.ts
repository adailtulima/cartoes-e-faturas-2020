import { Component, OnInit } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
// eslint-disable-next-line @typescript-eslint/no-unused-vars
import { FormBuilder, Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';

import { ICartao, Cartao } from 'app/shared/model/cartao.model';
import { CartaoService } from './cartao.service';
import { IPessoa } from 'app/shared/model/pessoa.model';
import { PessoaService } from 'app/entities/pessoa/pessoa.service';

@Component({
  selector: 'jhi-cartao-update',
  templateUrl: './cartao-update.component.html'
})
export class CartaoUpdateComponent implements OnInit {
  isSaving = false;
  pessoas: IPessoa[] = [];

  editForm = this.fb.group({
    id: [],
    numero: [],
    mesValidade: [],
    anoValidade: [],
    nome: [],
    codigoSeguranca: [],
    status: [],
    limiteCreditos: [],
    dono: []
  });

  constructor(
    protected cartaoService: CartaoService,
    protected pessoaService: PessoaService,
    protected activatedRoute: ActivatedRoute,
    private fb: FormBuilder
  ) {}

  ngOnInit(): void {
    this.activatedRoute.data.subscribe(({ cartao }) => {
      this.updateForm(cartao);

      this.pessoaService.query().subscribe((res: HttpResponse<IPessoa[]>) => (this.pessoas = res.body || []));
    });
  }

  updateForm(cartao: ICartao): void {
    this.editForm.patchValue({
      id: cartao.id,
      numero: cartao.numero,
      mesValidade: cartao.mesValidade,
      anoValidade: cartao.anoValidade,
      nome: cartao.nome,
      codigoSeguranca: cartao.codigoSeguranca,
      status: cartao.status,
      limiteCreditos: cartao.limiteCreditos,
      dono: cartao.dono
    });
  }

  previousState(): void {
    window.history.back();
  }

  save(): void {
    this.isSaving = true;
    const cartao = this.createFromForm();
    if (cartao.id !== undefined) {
      this.subscribeToSaveResponse(this.cartaoService.update(cartao));
    } else {
      this.subscribeToSaveResponse(this.cartaoService.create(cartao));
    }
  }

  private createFromForm(): ICartao {
    return {
      ...new Cartao(),
      id: this.editForm.get(['id'])!.value,
      numero: this.editForm.get(['numero'])!.value,
      mesValidade: this.editForm.get(['mesValidade'])!.value,
      anoValidade: this.editForm.get(['anoValidade'])!.value,
      nome: this.editForm.get(['nome'])!.value,
      codigoSeguranca: this.editForm.get(['codigoSeguranca'])!.value,
      status: this.editForm.get(['status'])!.value,
      limiteCreditos: this.editForm.get(['limiteCreditos'])!.value,
      dono: this.editForm.get(['dono'])!.value
    };
  }

  protected subscribeToSaveResponse(result: Observable<HttpResponse<ICartao>>): void {
    result.subscribe(
      () => this.onSaveSuccess(),
      () => this.onSaveError()
    );
  }

  protected onSaveSuccess(): void {
    this.isSaving = false;
    this.previousState();
  }

  protected onSaveError(): void {
    this.isSaving = false;
  }

  trackById(index: number, item: IPessoa): any {
    return item.id;
  }
}
