import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';

@NgModule({
  imports: [
    RouterModule.forChild([
      {
        path: 'pessoa',
        loadChildren: () => import('./pessoa/pessoa.module').then(m => m.ProjetoCartoesEFaturasPessoaModule)
      },
      {
        path: 'cartao',
        loadChildren: () => import('./cartao/cartao.module').then(m => m.ProjetoCartoesEFaturasCartaoModule)
      },
      {
        path: 'categoria',
        loadChildren: () => import('./categoria/categoria.module').then(m => m.ProjetoCartoesEFaturasCategoriaModule)
      },
      {
        path: 'endereco',
        loadChildren: () => import('./endereco/endereco.module').then(m => m.ProjetoCartoesEFaturasEnderecoModule)
      },
      {
        path: 'fatura',
        loadChildren: () => import('./fatura/fatura.module').then(m => m.ProjetoCartoesEFaturasFaturaModule)
      },
      {
        path: 'produto',
        loadChildren: () => import('./produto/produto.module').then(m => m.ProjetoCartoesEFaturasProdutoModule)
      },
      {
        path: 'pagamento',
        loadChildren: () => import('./pagamento/pagamento.module').then(m => m.ProjetoCartoesEFaturasPagamentoModule)
      }
      /* jhipster-needle-add-entity-route - JHipster will add entity modules routes here */
    ])
  ]
})
export class ProjetoCartoesEFaturasEntityModule {}
