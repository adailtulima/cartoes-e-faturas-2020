import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import './vendor';
import { ProjetoCartoesEFaturasSharedModule } from 'app/shared/shared.module';
import { ProjetoCartoesEFaturasCoreModule } from 'app/core/core.module';
import { ProjetoCartoesEFaturasAppRoutingModule } from './app-routing.module';
import { ProjetoCartoesEFaturasHomeModule } from './home/home.module';
import { ProjetoCartoesEFaturasEntityModule } from './entities/entity.module';
// jhipster-needle-angular-add-module-import JHipster will add new module here
import { MainComponent } from './layouts/main/main.component';
import { NavbarComponent } from './layouts/navbar/navbar.component';
import { FooterComponent } from './layouts/footer/footer.component';
import { PageRibbonComponent } from './layouts/profiles/page-ribbon.component';
import { ErrorComponent } from './layouts/error/error.component';

@NgModule({
  imports: [
    BrowserModule,
    ProjetoCartoesEFaturasSharedModule,
    ProjetoCartoesEFaturasCoreModule,
    ProjetoCartoesEFaturasHomeModule,
    // jhipster-needle-angular-add-module JHipster will add new module here
    ProjetoCartoesEFaturasEntityModule,
    ProjetoCartoesEFaturasAppRoutingModule
  ],
  declarations: [MainComponent, NavbarComponent, ErrorComponent, PageRibbonComponent, FooterComponent],
  bootstrap: [MainComponent]
})
export class ProjetoCartoesEFaturasAppModule {}
