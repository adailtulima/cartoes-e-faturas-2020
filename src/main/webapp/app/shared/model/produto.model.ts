import { Moment } from 'moment';
import { IFatura } from 'app/shared/model/fatura.model';

export interface IProduto {
  id?: number;
  nome?: string;
  local?: string;
  data?: Moment;
  valor?: number;
  fatura?: IFatura;
}

export class Produto implements IProduto {
  constructor(
    public id?: number,
    public nome?: string,
    public local?: string,
    public data?: Moment,
    public valor?: number,
    public fatura?: IFatura
  ) {}
}
