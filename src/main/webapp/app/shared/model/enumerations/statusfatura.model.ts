export const enum Statusfatura {
  GERADA = 'GERADA',
  ATRASADA = 'ATRASADA',
  CANCELADA = 'CANCELADA'
}
