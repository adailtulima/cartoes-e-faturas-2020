export const enum Status {
  BLOQUEADO = 'BLOQUEADO',
  LIBERADO = 'LIBERADO',
  ROUBADO = 'ROUBADO'
}
