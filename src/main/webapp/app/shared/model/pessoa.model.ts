import { Moment } from 'moment';
import { IEndereco } from 'app/shared/model/endereco.model';
import { ICartao } from 'app/shared/model/cartao.model';
import { ICategoria } from 'app/shared/model/categoria.model';

export interface IPessoa {
  id?: number;
  nome?: string;
  cpf?: string;
  dataNascimento?: Moment;
  dataCadastro?: Moment;
  endereco?: IEndereco;
  cartoes?: ICartao[];
  categoria?: ICategoria;
}

export class Pessoa implements IPessoa {
  constructor(
    public id?: number,
    public nome?: string,
    public cpf?: string,
    public dataNascimento?: Moment,
    public dataCadastro?: Moment,
    public endereco?: IEndereco,
    public cartoes?: ICartao[],
    public categoria?: ICategoria
  ) {}
}
