import { IFatura } from 'app/shared/model/fatura.model';
import { IPessoa } from 'app/shared/model/pessoa.model';
import { Status } from 'app/shared/model/enumerations/status.model';

export interface ICartao {
  id?: number;
  numero?: string;
  mesValidade?: number;
  anoValidade?: number;
  nome?: string;
  codigoSeguranca?: number;
  status?: Status;
  limiteCreditos?: number;
  faturas?: IFatura[];
  dono?: IPessoa;
}

export class Cartao implements ICartao {
  constructor(
    public id?: number,
    public numero?: string,
    public mesValidade?: number,
    public anoValidade?: number,
    public nome?: string,
    public codigoSeguranca?: number,
    public status?: Status,
    public limiteCreditos?: number,
    public faturas?: IFatura[],
    public dono?: IPessoa
  ) {}
}
