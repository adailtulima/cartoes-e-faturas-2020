import { Moment } from 'moment';
import { IPagamento } from 'app/shared/model/pagamento.model';
import { IProduto } from 'app/shared/model/produto.model';
import { ICartao } from 'app/shared/model/cartao.model';
import { Statusfatura } from 'app/shared/model/enumerations/statusfatura.model';

export interface IFatura {
  id?: number;
  dataProcessamento?: Moment;
  valor?: number;
  status?: Statusfatura;
  potuacao?: number;
  pagamento?: IPagamento;
  produtos?: IProduto[];
  cartao?: ICartao;
}

export class Fatura implements IFatura {
  constructor(
    public id?: number,
    public dataProcessamento?: Moment,
    public valor?: number,
    public status?: Statusfatura,
    public potuacao?: number,
    public pagamento?: IPagamento,
    public produtos?: IProduto[],
    public cartao?: ICartao
  ) {}
}
