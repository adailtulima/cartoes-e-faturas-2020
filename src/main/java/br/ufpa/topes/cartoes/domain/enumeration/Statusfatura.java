package br.ufpa.topes.cartoes.domain.enumeration;

/**
 * The Statusfatura enumeration.
 */
public enum Statusfatura {
    GERADA, ATRASADA, CANCELADA
}
