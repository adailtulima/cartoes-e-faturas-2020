package br.ufpa.topes.cartoes.domain;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import javax.persistence.*;

import java.io.Serializable;
import java.util.Objects;
import java.util.HashSet;
import java.util.Set;

import br.ufpa.topes.cartoes.domain.enumeration.Status;

/**
 * A Cartao.
 */
@Entity
@Table(name = "cartao")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
public class Cartao implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "numero")
    private String numero;

    @Column(name = "mes_validade")
    private Integer mesValidade;

    @Column(name = "ano_validade")
    private Integer anoValidade;

    @Column(name = "nome")
    private String nome;

    @Column(name = "codigo_seguranca")
    private Integer codigoSeguranca;

    @Enumerated(EnumType.STRING)
    @Column(name = "status")
    private Status status;

    @Column(name = "limite_creditos")
    private Double limiteCreditos;

    @OneToMany(mappedBy = "cartao")
    @Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
    private Set<Fatura> faturas = new HashSet<>();

    @ManyToOne
    @JsonIgnoreProperties("cartoes")
    private Pessoa dono;

    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getNumero() {
        return numero;
    }

    public Cartao numero(String numero) {
        this.numero = numero;
        return this;
    }

    public void setNumero(String numero) {
        this.numero = numero;
    }

    public Integer getMesValidade() {
        return mesValidade;
    }

    public Cartao mesValidade(Integer mesValidade) {
        this.mesValidade = mesValidade;
        return this;
    }

    public void setMesValidade(Integer mesValidade) {
        this.mesValidade = mesValidade;
    }

    public Integer getAnoValidade() {
        return anoValidade;
    }

    public Cartao anoValidade(Integer anoValidade) {
        this.anoValidade = anoValidade;
        return this;
    }

    public void setAnoValidade(Integer anoValidade) {
        this.anoValidade = anoValidade;
    }

    public String getNome() {
        return nome;
    }

    public Cartao nome(String nome) {
        this.nome = nome;
        return this;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public Integer getCodigoSeguranca() {
        return codigoSeguranca;
    }

    public Cartao codigoSeguranca(Integer codigoSeguranca) {
        this.codigoSeguranca = codigoSeguranca;
        return this;
    }

    public void setCodigoSeguranca(Integer codigoSeguranca) {
        this.codigoSeguranca = codigoSeguranca;
    }

    public Status getStatus() {
        return status;
    }

    public Cartao status(Status status) {
        this.status = status;
        return this;
    }

    public void setStatus(Status status) {
        this.status = status;
    }

    public Double getLimiteCreditos() {
        return limiteCreditos;
    }

    public Cartao limiteCreditos(Double limiteCreditos) {
        this.limiteCreditos = limiteCreditos;
        return this;
    }

    public void setLimiteCreditos(Double limiteCreditos) {
        this.limiteCreditos = limiteCreditos;
    }

    public Set<Fatura> getFaturas() {
        return faturas;
    }

    public Cartao faturas(Set<Fatura> faturas) {
        this.faturas = faturas;
        return this;
    }

    public Cartao addFaturas(Fatura fatura) {
        this.faturas.add(fatura);
        fatura.setCartao(this);
        return this;
    }

    public Cartao removeFaturas(Fatura fatura) {
        this.faturas.remove(fatura);
        fatura.setCartao(null);
        return this;
    }

    public void setFaturas(Set<Fatura> faturas) {
        this.faturas = faturas;
    }

    public Pessoa getDono() {
        return dono;
    }

    public Cartao dono(Pessoa pessoa) {
        this.dono = pessoa;
        return this;
    }

    public void setDono(Pessoa pessoa) {
        this.dono = pessoa;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here, do not remove

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof Cartao)) {
            return false;
        }
        return id != null && id.equals(((Cartao) o).id);
    }

    @Override
    public int hashCode() {
        return 31;
    }

    @Override
    public String toString() {
        return "Cartao{" +
            "id=" + getId() +
            ", numero='" + getNumero() + "'" +
            ", mesValidade=" + getMesValidade() +
            ", anoValidade=" + getAnoValidade() +
            ", nome='" + getNome() + "'" +
            ", codigoSeguranca=" + getCodigoSeguranca() +
            ", status='" + getStatus() + "'" +
            ", limiteCreditos=" + getLimiteCreditos() +
            "}";
    }
}
