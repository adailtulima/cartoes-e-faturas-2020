package br.ufpa.topes.cartoes.domain;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import javax.persistence.*;

import java.io.Serializable;
import java.util.Objects;
import java.time.LocalDate;
import java.util.HashSet;
import java.util.Set;

import br.ufpa.topes.cartoes.domain.enumeration.Statusfatura;

/**
 * A Fatura.
 */
@Entity
@Table(name = "fatura")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
public class Fatura implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "data_processamento")
    private LocalDate dataProcessamento;

    @Column(name = "valor")
    private Double valor;

    @Enumerated(EnumType.STRING)
    @Column(name = "status")
    private Statusfatura status;

    @Column(name = "potuacao")
    private Integer potuacao;

    @OneToOne
    @JoinColumn(unique = true)
    private Pagamento pagamento;

    @OneToMany(mappedBy = "fatura")
    @Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
    private Set<Produto> produtos = new HashSet<>();

    @ManyToOne
    @JsonIgnoreProperties("faturas")
    private Cartao cartao;

    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public LocalDate getDataProcessamento() {
        return dataProcessamento;
    }

    public Fatura dataProcessamento(LocalDate dataProcessamento) {
        this.dataProcessamento = dataProcessamento;
        return this;
    }

    public void setDataProcessamento(LocalDate dataProcessamento) {
        this.dataProcessamento = dataProcessamento;
    }

    public Double getValor() {
        return valor;
    }

    public Fatura valor(Double valor) {
        this.valor = valor;
        return this;
    }

    public void setValor(Double valor) {
        this.valor = valor;
    }

    public Statusfatura getStatus() {
        return status;
    }

    public Fatura status(Statusfatura status) {
        this.status = status;
        return this;
    }

    public void setStatus(Statusfatura status) {
        this.status = status;
    }

    public Integer getPotuacao() {
        return potuacao;
    }

    public Fatura potuacao(Integer potuacao) {
        this.potuacao = potuacao;
        return this;
    }

    public void setPotuacao(Integer potuacao) {
        this.potuacao = potuacao;
    }

    public Pagamento getPagamento() {
        return pagamento;
    }

    public Fatura pagamento(Pagamento pagamento) {
        this.pagamento = pagamento;
        return this;
    }

    public void setPagamento(Pagamento pagamento) {
        this.pagamento = pagamento;
    }

    public Set<Produto> getProdutos() {
        return produtos;
    }

    public Fatura produtos(Set<Produto> produtos) {
        this.produtos = produtos;
        return this;
    }

    public Fatura addProdutos(Produto produto) {
        this.produtos.add(produto);
        produto.setFatura(this);
        return this;
    }

    public Fatura removeProdutos(Produto produto) {
        this.produtos.remove(produto);
        produto.setFatura(null);
        return this;
    }

    public void setProdutos(Set<Produto> produtos) {
        this.produtos = produtos;
    }

    public Cartao getCartao() {
        return cartao;
    }

    public Fatura cartao(Cartao cartao) {
        this.cartao = cartao;
        return this;
    }

    public void setCartao(Cartao cartao) {
        this.cartao = cartao;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here, do not remove

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof Fatura)) {
            return false;
        }
        return id != null && id.equals(((Fatura) o).id);
    }

    @Override
    public int hashCode() {
        return 31;
    }

    @Override
    public String toString() {
        return "Fatura{" +
            "id=" + getId() +
            ", dataProcessamento='" + getDataProcessamento() + "'" +
            ", valor=" + getValor() +
            ", status='" + getStatus() + "'" +
            ", potuacao=" + getPotuacao() +
            "}";
    }
}
