package br.ufpa.topes.cartoes.domain.enumeration;

/**
 * The Status enumeration.
 */
public enum Status {
    BLOQUEADO, LIBERADO, ROUBADO
}
