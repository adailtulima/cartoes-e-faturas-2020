/**
 * View Models used by Spring MVC REST controllers.
 */
package br.ufpa.topes.cartoes.web.rest.vm;
